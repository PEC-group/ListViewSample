package com.example.listviewsample;

/**
 * Class representing a simple contact.
 */
public class Contact {

    // person's name
    private String name;
    // person's name
    private int phone;
    // resource id for image. eg. R.drawable.person_1
    private int image;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
