package com.example.listviewsample;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * An ArrayAdapter for the ListView to show the contact list(in our case)
 */
public class ContactListAdapter extends ArrayAdapter<Contact> {

    private Context context;
    // resource for the single row
    private int resourceId;
    // the contact list to show in the ListView
    private List<Contact> contactList;

    /**
     * Constructor for the adapter
     *
     * @param context     Context to get LayoutInflater from
     * @param resourceId  layout resource for the single row
     * @param contactList the contact list to show in the ListView
     */
    public ContactListAdapter(Context context, int resourceId, List<Contact> contactList) {
        super(context, resourceId, contactList);
        this.context = context;
        this.resourceId = resourceId;
        this.contactList = contactList;
    }

    /**
     * A Holder Class for holding the views in the single row layout. It is used for optimization.
     * (Research about optimization in ListView to know more)
     */
    class ViewHolder {
        ImageView imageViewContactPicture;
        TextView textViewName, textViewPhone;
    }

    @Override
    /**
     * Get a View that displays the data at the specified position in the data set.
     * Generally we create our inflater and inflate the layout for single row.
     */
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        // inflating and initializing views are done only once
        // this is done by checking the convertView in the above parameter.
        // It is null at first, then not null in next calls
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(resourceId, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.imageViewContactPicture = (ImageView) convertView.findViewById(R.id.imageview_contact_picture);
            viewHolder.textViewName = (TextView) convertView.findViewById(R.id.textview_name);
            viewHolder.textViewPhone = (TextView) convertView.findViewById(R.id.textview_phone);

            // setting the view holder instance in the convertView as tag
            convertView.setTag(viewHolder);
        } else {
            // getting the view holder out of covertView.
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // setting texts and image in the TextViews and ImageView
        viewHolder.textViewName.setText(getItem(position).getName());
        viewHolder.textViewPhone.setText(String.valueOf(getItem(position).getPhone()));
        viewHolder.imageViewContactPicture.setImageResource(getItem(position).getImage());

        // at last return the convertView
        return convertView;
    }

    @Override
    /**
     * Method for getting the Item at the given position from the list
     */
    public Contact getItem(int position) {
        return contactList.get(position);
    }
}
