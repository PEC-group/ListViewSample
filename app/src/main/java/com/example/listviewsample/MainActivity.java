package com.example.listviewsample;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Sample usage of ListView. Customization of the ListView content is done.
 */
public class MainActivity extends ActionBarActivity {

    // ListView to show contacts list
    private ListView listViewContacts;
    // An ArrayAdapter to show list of contacts in the ListView
    private ContactListAdapter contactListAdapter;

    // List to hold contacts (dummy contacts for now i.e self created)
    private List<Contact> contactList = new ArrayList<Contact>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // initialized ListView
        listViewContacts = (ListView) findViewById(R.id.listview_contacts);

        // creating dummy list of contact list
        createDummyList();

        // initializing the adapter passing the context, layout for single row of ListView
        // and the dummy contact list
        contactListAdapter = new ContactListAdapter(this, R.layout.single_row_contact_list, contactList);

        // setting adapter to the ListView. The adapter does all work in showing the list contents
        listViewContacts.setAdapter(contactListAdapter);

        // setting click listener in the items of the ListView
        listViewContacts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(MainActivity.this, contactList.get(position).getName(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * A method creating dummy list of contacts
     */
    private void createDummyList() {
        // creating object of custom class Contact
        Contact contact = new Contact();
        contact.setName("Person 1");
        contact.setPhone(1111111111);
        contact.setImage(R.drawable.person_1);
        // adding the first contact to the list
        contactList.add(contact);

        // reusing the previous contact object. However reinitializing is compulsory.
        contact = new Contact();
        contact.setName("Person 2");
        contact.setPhone(1222222222);
        contact.setImage(R.drawable.person_2);
        contactList.add(contact);

        contact = new Contact();
        contact.setName("Person 3");
        contact.setPhone(1234567890);
        contact.setImage(R.drawable.person_3);
        contactList.add(contact);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
